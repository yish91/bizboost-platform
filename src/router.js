import Vue from 'vue'
import VueRouter from 'vue-router'

import store from './store/store'

import Bids from './components/bids/Bids.vue'
import Loans from './components/loans/Loans.vue'
import LoanUpload from './components/loans/LoanUpload.vue'
import Portfolios from './components/portfolios/Portfolios.vue'
import Withdrawals from './components/withdrawals/Withdrawals.vue'
import BusinessOwnerProfile from './components/profile/BusinessOwnerProfile.vue'
import BusinessOwnerLoan from './components/loans/BusinessOwnerLoan.vue'
import Repayments from './components/repayments/Repayments.vue'
import Deposits from './components/deposits/Deposits.vue'
import SigninPage from './components/auth/signin.vue'

Vue.use(VueRouter)

const routes = [
    {path: '/', component: Loans},
    {path: '/login', component: SigninPage, name: "Login"},
    {path: '/loans', component: Loans},
    {path: '/upload', component: LoanUpload},
    {path: '/bids', component: Bids},
    {path: '/portfolios', component: Portfolios},
    {path: '/withdrawals', component: Withdrawals},
    {path: '/boprofile', component: BusinessOwnerProfile},
    {path: '/boloan', component: BusinessOwnerLoan},
    {path: '/repayments', component: Repayments},
    {path: '/deposits', component: Deposits}

];

const router = new VueRouter({mode: 'history', routes: routes})

router.beforeEach((to, from, next) => {
    if (to.name !== "Login" && (store.getters.user === null || store.getters.user === undefined)) {
        next({name: "Login"})
    } else {
        next()
    }
});

export default router