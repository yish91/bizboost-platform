import axios from "../../../axios-auth";

const state = {
    portfolios: []
};

const mutations = {
    'SET_PORTFOLIOS' (state, portfolios) {
        state.portfolios = portfolios
    }
};

const actions = {
    initPortfolios({commit}){
        axios.get('/portfolio', ).then(res => {
            commit('SET_PORTFOLIOS', res.data)
        })
            .catch(error => {
                console.log(error);
            })
    },
}
const getters = {
    portfolios(state){
        return state.portfolios
    }
}

export default {
    state,
    mutations,
    actions,
    getters
}