import axios from "../../../axios-auth";

const state = {
    bids: []
};

const mutations = {
    'SET_BIDS' (state, bids) {
        state.bids = bids
    }
};

const actions = {
    initBids({commit}){
        axios.get('/bid', ).then(res => {
            commit('SET_BIDS', res.data)
        })
            .catch(error => {
                console.log(error);
            })
    },
}
const getters = {
    bidPortfolio(state){
        return state.bids
    }
}

export default {
    state,
    mutations,
    actions,
    getters
}