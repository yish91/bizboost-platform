import axios from "../../../axios-auth";
import router from "@/router";

const state = {
    withdrawals: []
};

const mutations = {
    'SET_WITHDRAWALS' (state, withdrawals) {
        state.withdrawals = withdrawals
    }
};

const actions = {
    initWithdrawals({commit}){
        axios.get('/withdrawal', ).then(res => {
            commit('SET_WITHDRAWALS', res.data)
        })
            .catch(error => {
                console.log(error);
            })
    },
    withdraw({dispatch}, bid){
        const url = '/withdrawal'
        axios.post(url, bid).then(res => {
            if (res.status === 200){
                dispatch('initWithdrawals');
                router.push({ path : '/withdrawals' });
            }
        })
            .catch(error => {
                console.log(error);
            })
    }
}
const getters = {
    withdrawals(state){
        return state.withdrawals
    }
}

export default {
    state,
    mutations,
    actions,
    getters
}