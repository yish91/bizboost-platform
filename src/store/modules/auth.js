import axios from "../../../axios-auth";
import router from "../../router";

const state = {
    idToken: null,
    user: null,
    businessOwner: null,
    loginError: undefined
};

const mutations = {
    authUser(state, userData){
        state.idToken = userData.token;
        state.user = userData.user;
        state.businessOwner = userData.businessOwner
    },
    clearAuthUser(state){
        state.idToken = null;
        state.user = null;
        localStorage.clear();
    },
    errorUser(state, err){
        state.loginError = err
    },
    SET_BUSINESS_OWNER(state, data){
        state.businessOwner = data
    }
};

const actions = {
    login({commit, dispatch}, authData){
        commit('errorUser', undefined)
        axios.post('/user/login', {
            email: authData.email,
            password: authData.password
        }).then(res => {
            commit('authUser', {
                token: res.data.jwtToken,
                user: res.data.user,
                businessOwner: res.data.businessOwner
            });
            if(res.status === 200) {
                const now = new Date();
                const expirationDate = now + (60 * 60 * 24 * 1000);
                localStorage.setItem('token', res.data.jwtToken)
                localStorage.setItem('expiresIn', expirationDate)
                localStorage.setItem('user', JSON.stringify(res.data.user))
                localStorage.setItem('businessOwner', JSON.stringify(res.data.businessOwner))
                axios.defaults.headers.common['Authorization'] = res.data.jwtToken
                dispatch('initLoans');
                dispatch('initBids');
                dispatch('initPortfolios');
                dispatch('initWithdrawals');
                dispatch('initRepayments');
                dispatch('initDeposits');
                router.push({ path : '/' });
            }
        })
            .catch(error => {
                console.log(error)
                commit('errorUser', {
                    error
                })
            })
    },
    logout({commit}){
        commit('clearAuthUser');
        router.push({ path : '/login' });
    },
    tryAutoLogin({commit, dispatch}){
        const token = localStorage.getItem('token');
        if (!token){
            return
        }
        const expirationDate = localStorage.getItem('expiresIn');
        const now = new Date();
        if (now <= expirationDate){
            return
        }
        const user = JSON.parse(localStorage.getItem('user'));
        const businessOwner = JSON.parse(localStorage.getItem('businessOwner'));
        commit('authUser', {
            token: token,
            user: user,
            businessOwner: businessOwner
        });
        axios.defaults.headers.common['Authorization'] = token
        dispatch('initLoans');
        dispatch('initBids');
        dispatch('initPortfolios');
        dispatch('initWithdrawals');
        dispatch('initRepayments');
        dispatch('initDeposits');
        router.push({ path : '/' });
    },
    initBusinessOwner({commit}, uuid){
        axios.get('/user/bo/' + uuid).then(res => {
            commit('SET_BUSINESS_OWNER', res.data);
        })
            .catch(error => {
                console.log(error);
            })
    },
    uploadBusinessOwnerDocument({dispatch}, payload){
        let formData = new FormData();
        formData.append("file", payload.file);
        axios.post('/user/bo/' + payload.uuid + '/documents', formData, { headers: { 'Content-Type': 'multipart/form-data' }
        }).then(res => {
            console.log(res)
            dispatch('initBusinessOwner', payload.uuid);
            router.push({ path : '/' });
        })
            .catch(error => {
                console.log(error);
            })
    },
    updateBusinessOwner({dispatch}, bo){
        axios.post('/user/bo', bo
        ).then(res => {
            console.log(res)
            dispatch('initBusinessOwner', bo.uuid);
        })
            .catch(error => {
                console.log(error);
            })
    }
};

const getters = {
    loginError(state){
        return state.loginError;
    },
    user(state){
        return state.user;
    },
    businessOwner(state){
        return state.businessOwner;
    },
    jwtToken(state){
        return state.idToken;
    }
}

export default {
    state,
    mutations,
    actions,
    getters
}