import axios from "../../../axios-auth";

const state = {
    deposits: []
};

const mutations = {
    'SET_DEPOSITS' (state, deposits) {
        state.deposits = deposits
    }
};

const actions = {
    initDeposits({commit}){
        axios.get('/deposit', ).then(res => {
            commit('SET_DEPOSITS', res.data)
        })
            .catch(error => {
                console.log(error);
            })
    },
    deposit({dispatch}, bid){
        const url = '/deposit'
        axios.post(url, bid).then(res => {
            if (res.status === 200){
                dispatch('initDeposits');
            }
        })
            .catch(error => {
                console.log(error);
            })
    }
}
const getters = {
    deposits(state){
        return state.deposits
    }
}

export default {
    state,
    mutations,
    actions,
    getters
}