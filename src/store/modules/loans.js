import axios from "../../../axios-auth";
import router from "@/router";

const state = {
    loans: [],
    repayments: []
};

const mutations = {
    'SET_LOANS' (state, loans) {
        state.loans = loans
    },
    'SET_REPAYMENTS' (state, repayments) {
        state.repayments = repayments
    }
};

const actions = {
    initLoans({commit}){
        axios.get('/loan', ).then(res => {
            commit('SET_LOANS', res.data)
        })
            .catch(error => {
                console.log(error);
            })
    },
    bidLoan({dispatch}, bid){
        const url = '/loan/' + bid.loanId + '/bid'
        axios.post(url, bid).then(res => {
            if (res.status === 200){
                dispatch('initBids');
                router.push({ path : '/bids' });
            }
        })
            .catch(error => {
                console.log(error);
            })
    },
    closeLoan({dispatch}, close){
        const url = '/loan/' + close.loanId + '/close'
        axios.post(url).then(res => {
            if (res.status === 200){
                dispatch('initRepayments');
                dispatch('initLoans')
                dispatch('initWithdrawals');
                router.push({ path : '/repayments' });
            }
        })
            .catch(error => {
                console.log(error);
            })
    },
    repayLoan({dispatch}, repayment){
        const url = '/loan/' + repayment.id + '/repay'
        axios.post(url, repayment).then(res => {
            if (res.status === 200){
                dispatch('initRepayments');
                dispatch('initDeposits');
                router.push({ path : '/loans' });
            }
        })
            .catch(error => {
                console.log(error);
            })
    },
    createLoan({dispatch}, payload){
        axios.post('/loan', payload).then(res => {
            if (res.status === 200){
                dispatch('initLoans')
                router.push({ path : '/loans' });
            }
        })
            .catch(error => {
                console.log(error);
            })
    },
    initRepayments({commit}){
        axios.get('/repayment', ).then(res => {
            commit('SET_REPAYMENTS', res.data)
        })
            .catch(error => {
                console.log(error);
            })
    }

};

const getters = {
    loans: state => {
        return state.loans;
    },
    repayments: state => {
        return state.repayments
    }
};

export default {
    state,
    mutations,
    actions,
    getters
}