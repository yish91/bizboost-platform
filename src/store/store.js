import Vue from 'vue';
import Vuex from 'vuex'

import auth from './modules/auth';
import bids from './modules/bids';
import loans from './modules/loans';
import portfolio from './modules/portfolio';
import withdrawals from './modules/withdrawals';
import deposits from './modules/deposits';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        auth,
        bids,
        loans,
        portfolio,
        withdrawals,
        deposits
    }
});