import axios from 'axios';

const instance = axios.create({
    baseURL: "https://api.bizboost.live"
});

export default instance;