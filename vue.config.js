module.exports = {
    chainWebpack: config => {
        config
            .plugin('html')
            .tap(args => {
                args[0].title = "BizBoost - Boosting Your Investments";
                return args;
            })
    }
}
